

$(".custom-hamburger").click(function () {
    $(this).toggleClass("is-active");
});
$(".nav-link").click(function () {
    $(".custom-hamburger").toggleClass("is-active");
});

$(function () {
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        if (scroll >= 150) {
            $(".menu").addClass("bg-white nav-shadow");
            $(".nav-link").addClass("blue-links");
            $(".navbar-brand").wrapInner("<img src='images/fundadi-colors.png' class='header-logo' id='hideOnTop' alt=''>");
        } else {
            $(".menu").removeClass("bg-white nav-shadow");
            $(".nav-link").removeClass("blue-links");
            $("#hideOnTop").hide();
            $(".navbar-brand").wrapInner("<img src='images/header-logo.png' class='header-logo' alt=''>");
        }
    });
});
$('.navbar-nav .nav-item').click(function () {
    $('.navbar-nav .nav-item.active').removeClass('active');
    $(this).addClass('active');
});
$('.navbar-mobile .nav-item').click(function () {
    $('.navbar-mobile .nav-item.active').removeClass('active');
    $(this).addClass('active');
});
$(window).scroll(function () {
    var href = $(this).scrollTop();
    $('.link').each(function (event) {
        if (href >= $($(this).attr('href')).offset().top - 1) {
            $('.navbar-nav .nav-item.active').removeClass('active');
            $(this).addClass('active');
        }
    });
});
AOS.init();
document.querySelectorAll('a[href^="#"]').forEach(anchor => {
    anchor.addEventListener('click', function (e) {
        e.preventDefault();

        document.querySelector(this.getAttribute('href')).scrollIntoView({
            behavior: 'smooth'
        });
    });
});


